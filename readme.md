# Boolamp

Ambiente LAMP dockerizzato.

| Container | Versione Immagine|
| ------ | ------ |
| MariaDB | latest |
| Phpmyadmin | latest |
| Php & Apache | (custom) php 7.4 |
| Npm | (custom) node 12 |

## Requisiti mini
Docker Engine & Docker Compose installati sulla macchina. 
Per gli utenti Windows e Mac l'installazione del [Docker Desktop](https://www.docker.com/get-started) comprende già tutto.

## Utilizzo
E' sufficiente utilizzare il comando `docker-compose up` nella cartella dove è presente il file **docker-compose.yaml** per avviare i servizi.
La prima esecuzione richiederà più tempo, le immagini verrano scaricate dal Docker Hub e verranno creati i containers.
Nella cartella **projects** sarà possibile organizzare i propri progetti.

### Apache
Apache è esposto alla porta 80, quindi è raggiungibile all'url http://localhost, la document root punta alla cartella **projects**.
### PhpMyAdmin
PhpMyAdmin è esposto alla porta 8080, quindi è raggiungibile all'url http://localhost:8080
- **username**: root
- **password**: root (variabile d'ambiente presente nel docker-compose file: MYSQL_ROOT_PASSWORD)
### Npm
E' presente un container di node per utilizzare npm `docker-compose run npm bash` la working directory è projects.
## Comandi utili
Per avviare i containers
```sh
$ docker-compose up
```
Per stoppare i containers
```sh
$ docker-compose down
```
Per runnare un container ed attaccare un terminale bash.
```sh
$ docker-compose run npm bash
```
Per attaccare un terminale bash ad un container già in esecuzione.
```sh
$ docker-compose exec php-apache bash
```

## Immagini Custom
Nella cartella **.docker** sono presenti le semplici immagini docker custom per i container php-apache e node.
### Php Apache
Partendo dall'immagine base di **php:7.4-apache** vengono aggiunte le estensioni php per *pdo* e *mysqli* e viene incluso un semplice *custom.ini* che serve per sovrascrivere le variabili del php.ini di base.
### Node
Partendo dall'immagine base di **node:12** viene impostato l'utente "node" per evitare problemi di permessi quando vengono creati i file.
